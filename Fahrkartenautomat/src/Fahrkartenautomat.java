﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {while(true) {
       //Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       zuZahlenderBetrag = bestellungErfassen();
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
    }
    }
    
    
    private static void rückgeldAusgeben(double eingezahlterGesamtbetragLocal, double zuZahlenderBetragLocal) {
       double rückgabebetrag = eingezahlterGesamtbetragLocal - zuZahlenderBetragLocal;
       
       if(rückgabebetrag > 0.0)
       {
    	   druckeMuenze(rückgabebetrag);
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    
    }
	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze = 0;
		Scanner tastatur = new Scanner(System.in);
		eingezahlterGesamtbetrag = 0.0;
		   while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		   {
			  
			   System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			  
			   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			   boolean bedingung = false;
			   while (bedingung == false) {
			   eingeworfeneMünze = tastatur.nextDouble();
			   if(eingeworfeneMünze >= 0.05 && eingeworfeneMünze <= 2.0) {
				   bedingung = true;
			   }
			   else System.out.println("Die Münze wurde nicht erkannt. Werfen sie eine neue ein.");
			   }
		       eingezahlterGesamtbetrag += eingeworfeneMünze;
		   }
		return eingezahlterGesamtbetrag;
	}

	private static double bestellungErfassen() {
		int [] num = {1,2,3,4,5,6,7,8,9,10};
		double [] preise = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
		String [] bezeichner = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC",
				"Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen Tageskarte Berlin",
				"Kleingruppen Tageskarte Berlin BC","Kleingruppen Tageskarte Berlin ABC"};
		for(int i = 0;i<10;i++) {
			System.out.printf("%-2d %-35s %.2f EUR \n",num[i],bezeichner[i],preise[i]);
		}
		System.out.println("Wählen sie bitte einen Fahrschein.");
		int ticketwahl;
		int ticketAnzahl = 0;
		double zuZahlenderBetrag  = 0;
		boolean gueltig = false;
		
		Scanner tastatur = new Scanner(System.in);
		boolean bedingung = false;
		while(bedingung == false) {
		ticketwahl = tastatur.nextInt();
		
		if(0<ticketwahl && ticketwahl<10) {
			zuZahlenderBetrag = preise[ticketwahl-1];
			bedingung = true; 
			System.out.println("Sie haben "+ bezeichner[ticketwahl-1]+ " gewählt.");
		}
		else System.out.println("Sie haben keine gültige Eingabe getätigt. Versuchen sie es bitte erneut.");
		}
		
		
		while(gueltig == false) {
		System.out.print("Anzahl der Tickets:");
		 ticketAnzahl = tastatur.nextInt();
		 if(ticketAnzahl > 10 || ticketAnzahl < 1) {
			 ticketAnzahl = 1;
			 System.out.println("Sie haben eine ungültige Eingabe der Ticketanzahl getätigt. Bitte geben sie einen gültigen Wert ein.");
		 }
		 else gueltig = true;
		 }
		 
		zuZahlenderBetrag *= ticketAnzahl;
		return zuZahlenderBetrag;
	}
	private static void fahrkartenAusgeben(){
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);
	       }
	       System.out.println("\n");
	       }
	
	
	private static void warte(int x) {
		try {
			Thread.sleep(x);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			
	return;
		
	}
	
	
	
	// komplexe Methode welche die Münzen visuell ausgibt
	public static void druckeMuenze(double betrag){
	int muenzenZahl = 0;
	int muenzarray [] = {0,0,0,0,0,0};
	int betragsarray [] = {2,1,50,20,10,5};
	while(betrag > 0){
	if((betrag-2.0) > 0){
			betrag = betrag-2;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray [0] += 1;
			muenzenZahl += 1;
	}
	else if((betrag-1.0) >= 0){
			betrag = betrag-1;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray[1] +=1;
			muenzenZahl += 1;
	}
	else if((betrag-0.5) >= 0){
			betrag = betrag-0.5;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray[2] +=1;
			muenzenZahl += 1;
	}

	else if((betrag-0.2) >= 0){
			betrag = betrag-0.2;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray[3] +=1;
			muenzenZahl += 1;
	}
	else if((betrag-0.1) >= 0){
			betrag = betrag-0.1;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray[4] +=1;
			muenzenZahl += 1;
	}	
	else if((betrag-0.05) >= 0){
			betrag = betrag-0.05;
			betrag = Math.round(betrag * 100)/100.0f;
			muenzarray[5] +=1;
			muenzenZahl += 1;
	}			
	}


	int one = 0;
	int two = 0;
	int three = 0;

	while(muenzenZahl>0){
		for(int j = 0;j<3; j++) {
			for (int i = 0; i < betragsarray.length; i++) {
				if (muenzarray[i] > 0){
					switch(j){
						case(1):
							one = betragsarray[i];
							muenzarray[i]-=1;
							j+=1;
							i-=1;
							break;
						case(2):
							two = betragsarray[i];
							muenzarray[i]-=1;
							j+=1;
							i-=1;
							break;
						case(3):
							three = betragsarray[i];
							muenzarray[i]-=1;
							j+=1;
							i-=1;
							break;
					}
				}
			}
		}

		String one_str = "EURO";
		String two_str = "EURO";
		String three_str = "EURO";

		if(one > 2) one_str = "CENT";
		if(two > 2) two_str = "CENT";
		if(three > 2) three_str = "CENT";




		if (muenzenZahl>=3){
			muenzenZahl-=3;
		System.out.printf("  ***     ***     ***  ");
		System.out.println("");
		System.out.printf(" *   *   *   *   *   *  ");
		System.out.println("");
		System.out.printf("* %2d  * * %2d  * * %2d  *  ",one,two,three);
		System.out.println("");
		System.out.printf("* %4s* * %4s* * %4s*  ",one_str,two_str,three_str);
		System.out.println("");
		System.out.printf(" *   *   *   *   *   *  ");
		System.out.println("");
		System.out.printf("  ***     ***     ***  ");
		System.out.println("");
		}
		else if (muenzenZahl>=2){
			muenzenZahl-=2;
			System.out.printf("  ***     ***     ");
			System.out.println("");
			System.out.printf(" *   *   *   *     ");
			System.out.println("");
			System.out.printf("* %2d  * * %2d  *  ",one,two);
			System.out.println("");
			System.out.printf("* %4s* * %4s*  ",one_str,two_str);
			System.out.println("");
			System.out.printf(" *   *   *   *   ");
			System.out.println("");
			System.out.printf("  ***     ***     ");
			System.out.println("");
		}
		else{
			muenzenZahl-=1;
			System.out.printf("  ***  ");
			System.out.println("");
			System.out.printf(" *   *  ");
			System.out.println("");
			System.out.printf("* %2d  * ",one);
			System.out.println("");
			System.out.printf("* %4s* ",one_str);
			System.out.println("");
			System.out.printf(" *   *   ");
			System.out.println("");
			System.out.printf("  ***     ");
			System.out.println("");
		}
	}
	
	}
}
	
