
public class Fahrenheit {
public static void AusgabeAufgabe_3() {
	//Erstellung Tabellenkopf mit Formatierung
	System.out.printf("\n%-12s", "Fahrenheit");
	System.out.print('|');
	System.out.printf("%10s", "Celsius");
	System.out.print("\n------------------------");
	
	//Uebergabe der Werte in ein Array
	float[] values = new float[] {
			-20.0f,-28.8889f,
			-10.0f,-23.3333f,
			0.0f,-17.7778f,
			20.0f,-6.6667f,
			30.0f,-1.1111f};
	
	
	
	//Ausgabe Werte �ber Schleife
	for(int i= 0;i<10;i++) {
		System.out.printf("\n%+-12.2f", values[i]);
		System.out.print('|');
		System.out.printf("%10.2f", values[i+1]);
		i = i+1;
	}
	
	
	
	}

}
